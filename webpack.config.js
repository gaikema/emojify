var webpack = require('webpack');
var path = require('path');

module.exports = {
	context: __dirname + "/src",
	entry: "./main.js",
	output: {
		path: __dirname + "/dist",
		filename: "bundle.js"
	},
	plugins: [
		//new webpack.optimize.UglifyJsPlugin({mangle: false})
	],
	target: 'web',
	module: {
		loaders: [
			{ 
				test: /\.json$/,				 
				//include: [path.resolve(__dirname, "not_exist_path")],
				loader: "json-loader" 
			},
			{ 
				test: /\.tsx?$/, 
				loader: "ts-loader" 
			}
		]
	}
};