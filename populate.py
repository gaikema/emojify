# https://github.com/TexAgg/texmoji

import json
import os
import urllib
from bs4 import BeautifulSoup
import os
import re

class data_getter:

	def __init__(self, url):
		self.url = url
		r = urllib.urlopen(self.url).read()
		self.soup = BeautifulSoup(r, "html.parser")
		self.emojis = []

	def scrape(self):
		rows = self.soup.find_all("tr")

		for row in rows:
			cols = row.find_all("td")
			if len(cols) != 0:
				# http://stackoverflow.com/a/16720752/5415895
				self.emojis.append(re.sub("(U\+)", "0x", cols[1].string.split(' ')[0]))

		return self.emojis

# http://unicode.org/emoji/charts/full-emoji-list.html
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/

def main():
	dg = data_getter('http://unicode.org/emoji/charts/full-emoji-list.html')
	data = dg.scrape()

	directory = "data"
	if not os.path.exists(directory):
	    os.makedirs(directory)
	with open('data/emojis.json', 'w') as outfile:
	    json.dump(data, outfile)

if __name__ == "__main__":
	main()
