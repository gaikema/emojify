BUNDLE = src/main.js src/Emojify.ts webpack.config.js package.json

.PHONY: all
all: dist/bundle.js lib/*

lib/*: src/Emojify.ts
	tsc

dist/bundle.js: data/emojis.json $(BUNDLE)
	webpack

data/emojis.json: populate.py
	python populate.py

.PHONY: test
test: lib/*
	node tests/test.js

.PHONY: release
release: data/emojis.json
	yarn install
	webpack
	yarn install --production