/// <reference path="require.d.ts" />

/**
 * A class for adding emojis after each space in a text.
 * 
 * @export
 * @class Emojify
 */
export class Emojify
{
	private static codes = <string[]>require('../data/emojis.json');
	private text: string;

	constructor(text: string)
	{
		this.text = text;
	}

	private static zip<T>(a: Array<T>, b: Array<T>): T[]
	{
		// http://stackoverflow.com/a/22015771/5415895
		let c = a.map(function (e, i) {
			return [e, b[i]];
		});
		// http://stackoverflow.com/a/10865042/5415895
		return [].concat.apply([], c);
	}

	public static printCodes(): void
	{
		console.log(Emojify.codes);
	}

	/**
	 * Given an array arr of length n, returns a new array
	 * with n non-unique elements selected from arr.
	 * 
	 * @private
	 * @static
	 * @template T
	 * @param {Array<T>} arr
	 * @returns {Array<T>}
	 * 
	 * @memberOf Emojify
	 */
	private static sample<T>(arr: Array<T>): Array<T>
	{
		let n = arr.length;
		let res = new Array<T>();
		for (let i = 0; i < n; i++)
		{
			let index = Math.floor(Math.random()*n);
			res.push(arr[index]);
		}

		return res;
	}

	public emojify(): string
	{
		let prep: string[] = this.text.split(" ");
		let emojis = Emojify.sample(Emojify.codes.map(function(e){
			// http://stackoverflow.com/a/22315491/5415895
			return String.fromCodePoint(Number(e));
		}));
		let c = Emojify.zip(prep, emojis);

		return c.join(" ");
	}
}