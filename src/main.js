var emojify = require("./Emojify.ts");
var twemoji = require("twemoji");

var form = document.getElementById('main-form');
if (form.attachEvent) {
    form.attachEvent("submit", processForm);
} else {
    form.addEventListener("submit", processForm);
}

// http://stackoverflow.com/a/5384732/5415895
function processForm(e) {
    if (e.preventDefault) e.preventDefault();

	// Trim extra whitespace.
	var inputText = document.getElementsByName('input-text')[0].value.trim();
	var e = new emojify.Emojify(inputText);
	var result = e.emojify();

	var outputElement = document.getElementById('output');
	outputElement.innerHTML = "<div>" + twemoji.parse(result) + "</div>";
	resizeEmojis();
	var copyable = getOutputTextarea();
	copyable.value = result;
	outputElement.appendChild(copyable);

    // You must return false to prevent the default form behavior.
    return false;
}

// Otherwise the emojis are too big.
function resizeEmojis() {
	var imgs = document.getElementsByClassName('emoji');
	for (var i = 0; i < imgs.length; i++) {
		imgs[i].setAttribute('style', 'width:16px;');
	}
}

// Create a readonly textarea.
function getOutputTextarea() {
	var textarea = document.createElement('textarea');
	textarea.setAttribute('cols', '40');
	textarea.setAttribute('rows', '20');
	textarea.setAttribute('readonly', 'readonly');
	// http://stackoverflow.com/a/4067488/5415895
	textarea.setAttribute('onclick', 'this.select()');
	return textarea;
}