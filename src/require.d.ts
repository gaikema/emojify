/*
	https://github.com/TypeStrong/ts-loader/issues/30#issuecomment-128456710
*/
declare var require: {
    <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void): void;
    ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void) => void;
};