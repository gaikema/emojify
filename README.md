[![npm version](https://badge.fury.io/js/%40gaikema%2Femojify.svg)](https://badge.fury.io/js/%40gaikema%2Femojify)

# Emojify
Insert an emoji between every word.

---

## Usage
```
yarn install @gaikema/emojify
```
You have to include the `@gaikema` prefix, since `emojify` is another package.

### Node
```
var Emojify = require("@gaikema/emojify");

var emojifyTest = new Emojify.Emojify("This is a test!");
console.log(emojifyTest.emojify());
```

### Webpack
Usage is more or less the same, but you will need to install some extra loaders from the `devDependencies`.